// swift-tools-version:5.1

import PackageDescription
let package = Package(
    name: "MMMaterialDesignSpinner",
    platforms: [
        .iOS(.v8),
        .tvOS(.v9)
    ],
    products: [
        .library(
            name: "MMMaterialDesignSpinner",
            targets: ["MMMaterialDesignSpinner"])
        ],
    targets: [
        .target(
            name: "MMMaterialDesignSpinner",
            dependencies: [],
            path: "Pod/Classes")
    ]
)
